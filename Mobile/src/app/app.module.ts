import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { HttpClientModule } from '@angular/common/http';
//import { UtilService } from './../services/util.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { TabsPage } from '../pages/tabs/tabs';

export const config = {
    apiKey: "AIzaSyCiyP5AfhhXolK2WYo6m8uDRi-mFEDXxes",
    authDomain: "afazeresff.firebaseapp.com",
    databaseURL: "https://afazeresff.firebaseio.com",
    projectId: "afazeresff",
    storageBucket: "afazeresff.appspot.com",
    messagingSenderId: "505852334794"
  }
 
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: 'Voltar'
    }),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
