import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.scss']
})
export class ProgressbarComponent implements OnInit {


  @Input() ProgressbarPercentage: number;

  constructor() { }

  ngOnInit() {
  }


  get getBarPercentage() {
    const style = {
      'width': this.ProgressbarPercentage + '%'
    }
    return style;
  }
}
