import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
@Injectable()
export class GetDataService {

  constructor(public db: AngularFireDatabase) { }


  getDataLista(destino) {
    const retorno = [];
    const promise = new Promise((resolve, reject) => {
      this.db.list(destino).valueChanges().subscribe((s) => {
        s.forEach(element => {
          retorno.push(element);
        });
        resolve(retorno);
      });
    });
    return promise;
  }
  getDataObjeto(destino) {
    const retorno = [];
    const promise = new Promise((resolve, reject) => {
      this.db.object(destino).valueChanges().subscribe((s: any) => {
        s.forEach(element => {
          retorno.push(element);
        });
        resolve(retorno);
      });
    });
    return promise;
  }
  checkData(data) {
    if (data === undefined) {
      // console.log('Fuck Yes!', data)
      return false;
    } else {
      //  console.log('Meeehhh', data)
      return true;
    }
  }
  writeData(destino, data) {
    const promise = new Promise((resolve, reject) => {
      this.db.object(destino).set(
        data
      ).then((s) => {
        console.log(s);
        resolve(data);
      });
    });
    return promise;
  }
  updateData(destino, data) {
    const promise = new Promise((resolve, reject) => {
      this.db.object(destino).update(
        data
      ).then((s) => {
        console.log(s);
        resolve(data);
      });
    });
    return promise;
  }
}
