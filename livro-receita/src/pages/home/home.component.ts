import { GetDataService } from './../../services/getData.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
Menu: any[];
  constructor(public dataSrv: GetDataService) {

    this.dataSrv.getDataLista('Receitas').then((s: any[]) => {
      this.Menu = [];
      this.Menu = s;
      console.log(this.Menu);

    });

  }



  ngOnInit() {
  }

}
